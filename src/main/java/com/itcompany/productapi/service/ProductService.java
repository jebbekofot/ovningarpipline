package com.itcompany.productapi.service;

import com.itcompany.productapi.entity.Product;
import com.itcompany.productapi.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    public Product getProduct(String name) {
        return productRepository.getByName(name);
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }
}
