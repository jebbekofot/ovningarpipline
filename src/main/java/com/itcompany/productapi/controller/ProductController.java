package com.itcompany.productapi.controller;

import com.itcompany.productapi.entity.Product;
import com.itcompany.productapi.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @GetMapping("/product")
    public Product getProduct(@RequestParam String name) {
        return productService.getProduct(name);
    }

    @PostMapping(consumes = "application/json")
    public Product addProduct(@RequestBody Product product) {
        Product prod = productService.addProduct(product);
        return prod;
    }

}
